package me.vad;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Main {
    static byte[] sensitiveData;
    
    public static void main(String[] args) {
        sensitiveData = "mySecurelyStoredPassword€€€".getBytes();
        // the goal is to not leave this data in memory
        // Java Docs of Crypto also recommend to use an array instead of String obj
        // for this purpose.
        // So the only task left is to wipe the data on VM Exit/OOM. SIGKILL is probably impossible
        // But thats the best we can do to keep it out of memory dumps etc.
        
        System.out.println("Starting up...");
        
        
        // Approach 1: Runtime Hooks
        // Resolution: The hook is run, but only AFTER a heap dump has been performed
        // -> Sensitive data ends up in OOM Dumps, and any other heap dumps
        Runtime runtime = Runtime.getRuntime();
        runtime.addShutdownHook(new Thread(() -> {
            Arrays.fill(sensitiveData, (byte) 0);
            System.out.println("Shutdown hook fired!");
        }));
        
        // Approach 2: Background thread to catch VMExit/OOM
        // Resolution: Doesn't get triggered on VM Exit/OOM in any way
        Thread bg = new Thread(() -> {
            try {
                while (true) {
                    Thread.sleep(30000);
                }
            } catch (InterruptedException ex) {
                System.out.println("Background thread catch fired: " + ex.getCause());
            } finally {
                Arrays.fill(sensitiveData, (byte) 0);
                System.out.println("Background thread fired!");
            }
            
        });
        
        System.out.println("Preparing the crash:");
        Random rand = ThreadLocalRandom.current();
        // Cause OOM
        long[] big = new long[1_000_000_000];
        
        for (int i = 0; i < big.length; i+=64) {
            big[i] = rand.nextInt();
        }
        System.out.println(big[rand.nextInt(big.length)]);
        
        System.out.println("Array successfully allocated!");
    }
}
