# Java: clear sensitive data on exit?

**In this testing snippet I tried to see if it's possible to clear the Java heap of any "sensitive" data upon exit.**

Short answer: Not possible.

### Example: an `OutOfMemory` error occurs.

- `Runtime.addShutdownHook()` is only called after the heap has been dumped to disk (after allocation failure) and before program exit. Clearing the byte[] array before process exits is of little use, since the OS would clear the memory regions ASAP by itself: Except for bugs (7-Zip, Windows 10 and Large Memory Pages).

- The second thread with infinite `sleep()` never enters its try-catch block: useless.

- Just for the sake of testing I decided to catch `OutOfMemoryError` in the main thread that does the allocation: Useless, heap is dumped before try-catch is run.

This is the output with `-XX:+HeapDumpOnOutOfMemoryError`: 

```
Starting up...
Preparing the crash:
java.lang.OutOfMemoryError: Java heap space
Dumping heap to java_pid7298.hprof ...
Heap dump file created [1406452 bytes in 0.008 secs]
Shutdown hook fired!
Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
	at me.vad.Main.main(Main.java:49)
```

As you can see the ShutdownHook is called too late and sensitive data ends up in dump.

---

Good news is: on Unix the heapdump has `rw- --- ---` permission - only the current user can read the dump (See comment in JDK-6451099).

Bad news: Hotspot JVM has internal switches for GC-before-dump (heapDumper.cpp:1990) and even though it emits a JFR event there, no way to enable that switch.

Though maybe you could somehow hack and provoke a dump error ;)

---

One possible hack is to stash sensitive data in OS buffers to make it invisible to heap dumps and they'd be cleared on process exit.

No other Java-side solutions appear to exist (Stackoverflow Q/5482506). This problem needs assistance from JVM guys.

One final thought: Normally you can't dump heap unless have root rights. Should the user himself be able to dump the heap of his own process? If not, detect the usage of `-XX:+HeapDumpOnOutOfMemoryError`. 